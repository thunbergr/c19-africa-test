from pipeline import pipeline
import pandas as pd
import config as cfg

# Assert that the data pull actually pulls the right file
def test_kaggle_data_extract():
    if not pipeline.data_file_exists():
        pipeline.extract_from_kaggle()

    assert pipeline.data_file_exists()

# Assert that function returns df with the correct columns
def test_covid_africa_si_table_correct_columns():
    if not pipeline.data_file_exists():
        pipeline.extract_from_kaggle()

    assert set(['Country', 'SI']).issubset(pipeline.create_severity_index_table().columns)

# Assert that function returns df with the correct columns
def test_covid_africa_metrics_table_correct_columns():
    if not pipeline.data_file_exists():
        pipeline.extract_from_kaggle()
    assert set(['Country', 'metric_tr_sum', 'metric_td_median', 'metric_tt_std']).issubset(pipeline.create_metrics_table().columns)

# Assert that no values are NaN in the functions resulting df
def test_data_si_is_not_nan():
    if not pipeline.data_file_exists():
        pipeline.extract_from_kaggle()

    df = pipeline.create_severity_index_table()
    print(df)
    assert not df.isnull().values.any()

# Assert that no values are NaN in the functions resulting df
def test_data_metrics_is_not_most_nan():
    if not pipeline.data_file_exists():
        pipeline.extract_from_kaggle()

    df = pipeline.create_metrics_table()
    assert df['metric_tr_sum'].isna().sum()/df['metric_tr_sum'].notna().sum() <= 0.95
    assert df['metric_td_median'].isna().sum()/df['metric_td_median'].notna().sum() <= 0.95
    assert df['metric_tt_std'].isna().sum()/df['metric_tt_std'].notna().sum() <= 0.95
