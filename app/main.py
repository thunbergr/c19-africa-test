import subprocess
from pipeline import pipeline
import config as cfg

menu = {
    1: 'Run pipeline',
    2: 'Run unit-tests',
    3: 'Exit'
}

def print_menu():
    """ Prints the terminal menu

    This function aims to solve a basic requirement which was to make the pipeline
    runnable from the terminal using a main script file.
    """

    menu_msg = 'Welcome to this Siemens coding test. Coding was done by Rasmus Thunberg. Visit pgAdmin \non http://localhost:5050 to watch the resulting tables (credentials in docker-compose.yml).'
    print('\n\n{0}{1}\n{2}\n{1}\n'.format(cfg.color.HEADER, "-"*len(menu_msg.split('\n')[0]), menu_msg))
    for key in menu.keys():
        print (key, '-', menu[key] )

if __name__=='__main__':
    
    while(True):
        print_menu()

        try:
            choice = int(input('Enter your choice: '))
        except:
            print('\nInvalid input, please enter a number ...')

        if choice == 1:
            pipeline.run()
            input(f'\n{cfg.color.STANDARD}Press any key to continue... ')
        elif choice == 2:
            commands = [
            'python -m pytest --no-header -vv tests/']
            result = subprocess.run(' && '.join(commands), shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)
            print(f'{cfg.color.STANDARD}{result.stdout}')
            print(f'{cfg.color.FAIL}{result.stderr}')
            input(f'\n{cfg.color.STANDARD}Press any key to continue... ')
        elif choice == 3:
            exit()
        else:
            print('\nInvalid option, please enter a number between 1 and 3.')