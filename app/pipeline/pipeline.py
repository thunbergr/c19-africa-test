import pandas as pd
from sqlalchemy import create_engine
from sqlalchemy_utils import database_exists, create_database
import config as cfg
import subprocess
from pathlib import Path

# Helper function
def data_file_exists():
    path = Path(f'{cfg.PROJECT_ROOT}/in/covid_africa.csv')
    return path.is_file()

def upload_to_postgres(df, table):
    """ Uploads the the requested tables

    This function aims to satistfy a requirement for bonus-point. It simple 
    create/truncates the produced tables to a local postgresSQL. Change the 
    config to reproduce, since it now is populated with my own local configs.
    """
    url = f'postgresql://{cfg.postgres["user"]}:{cfg.postgres["password"]}@{cfg.postgres["host"]}:{cfg.postgres["port"]}/{cfg.postgres["db"]}'
    engine = create_engine(url)
    if not database_exists(url):
        create_database(url)

    df.to_sql(table, engine, if_exists='replace', index=False)
    return df

def extract_from_kaggle():
    """ Extracts data from Kaggle API

    This function executes bash-commands which pulls the data using Kaggle CLI.
    It also unzips the data and removes the pulled zip-file.
    """

    commands = [
    f'cd {cfg.PROJECT_ROOT}/in',
    'kaggle datasets download -d anandhuh/covid-in-african-countries-latest-data',
    'unzip -o *.zip',
    'rm *.zip',
    f'cd {cfg.PROJECT_ROOT}',
    ]
    result = subprocess.run(' && '.join(commands), shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)
    print(f'{cfg.color.STANDARD}{result.stdout}')
    print(f'{cfg.color.FAIL}{result.stderr}{cfg.color.STANDARD}')
    
def create_severity_index_table():
    """ Creates the severity index table

    This function aims to solve the first test, which was to create a 
    severity index. It reads the .csv-file, produces the SI-column and 
    creates a df with the requested table structure. 
    """

    # Create Dataframe from file
    df = pd.read_csv(f'{cfg.PROJECT_ROOT}/in/covid_africa.csv')

    # Create a covid severity index for each country 
    df['death_rank'] = df['Deaths/1 mil population '].rank()
    df['total_cases_rank'] = df['Total Cases/1 mil population'].rank()
    df['SI'] = df['death_rank'] * df['total_cases_rank']

    # Create and return a df with the ranking of all countries according to the severity index in ascending manner, table should include country’s name and severity index
    t1_content = ['Country', 'SI']
    return df[t1_content].sort_values('SI').reset_index(drop=True)

def create_metrics_table():
    """ Creates the metrics table

    This function aims to solve the second test, which was to create a 
    several metrics for each country. It reads the .csv-file, produces each
    and every metric and creates a df with the requested table structure.
    """

    # Create Dataframe from file
    df = pd.read_csv(f'{cfg.PROJECT_ROOT}/in/covid_africa.csv')

    ### Creating the metrics ###

    # Creating df with country-index for easy access
    country_map = df.copy().set_index('Country')

    # Metric 1: The number of total recovered in that country divided by the total recovered in the whole data set.
    df['metric_tr_sum'] = df['Country'].apply(lambda c: country_map.loc[c, 'Total Recovered'] / country_map['Total Recovered'].sum())

    # Metric 2: The total deaths of that country divided by the median of total deaths form the whole data set
    df['metric_td_median'] = df['Country'].apply(lambda c: country_map.loc[c, 'Total Deaths'] / country_map['Total Deaths'].median())

    # Metric 3: The total tests performed for that country divided by the standard deviation of the total tests from the whole data set
    df['metric_tt_std'] = df['Country'].apply(lambda c: country_map.loc[c, 'Total Tests'] / country_map['Total Tests'].std())

    t2_content = ['Country', 'metric_tr_sum', 'metric_td_median', 'metric_tt_std']
    return df[t2_content]

def run():
    t1_name = 'covid_africa_si'
    t2_name = 'covid_africa_metrics'
    print(f'\n\n{cfg.color.SUCCESS}Starting pipeline\n')

    print(f'{cfg.color.STANDARD}Extracting from Kaggle API...', end=' ')
    if not data_file_exists():
        extract_from_kaggle()

    print(f'{cfg.color.SUCCESS}Done\n{cfg.color.STANDARD}Creating SI table...', end=' ')
    df_t1 = create_severity_index_table()

    print(f'{cfg.color.SUCCESS}Done\n{cfg.color.STANDARD}Uploading table [{t1_name}] to {cfg.postgres["host"]}:{cfg.postgres["port"]}...', end=' ')
    upload_to_postgres(df_t1, t1_name)

    print(f'{cfg.color.SUCCESS}Done\n{cfg.color.STANDARD}Creating metrics table...', end=' ')
    df_t2 = create_metrics_table()

    print(f'{cfg.color.SUCCESS}Done\n{cfg.color.STANDARD}Uploading table [{t2_name}] to {cfg.postgres["host"]}:{cfg.postgres["port"]}...', end=' ')
    upload_to_postgres(df_t2, t2_name)

    print(f'{cfg.color.SUCCESS}Done\n\n{cfg.color.SUCCESS}Pipeline executed successfully')

if __name__ == "__main__":
  run()