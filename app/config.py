import os
''' Config the pipeline '''

# PostgreSQL config
postgres = {
    'host' : 'postgres',
    'port' : '5432',
    'user' : 'siemensemployee',
    'password' : 'SuperLongSecretPassword',
    'db': 'de_coding_test'
}

PROJECT_ROOT = os.path.dirname(os.path.abspath(__file__))

class color():
    SUCCESS = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    HEADER = '\033[95m'
    STANDARD = '\033[0m'