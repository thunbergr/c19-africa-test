# Data Engineering test by Thunberg

#### A few design comments:
- A covid_africa.csv-file is included in the in-folder. The reason for this is that I did not want to use Kaggle API CLI inside the docker-container during run, because AFAIK it would be nessessery to copy in personal kaggle-tokens. If the file is missing, it will try to use kaggle to fetch it, but that will only work if the project is run on a local environment with Kaggle CLI installed. I did not put in the time to try work around this because it think that the code itself proves the point (the bonus-point requirement).
- When run with docker-compose (as instructed below), a postgres-db is set up and will be accessible though pgAdmin. 
- When connecting to the db though pgAdmin, remember that the credentials is included in the docker-compose.yml-file, and the host will be the Service-name ('postgres'). The credentials should be put somewhere else in a real project, but no requirement touched upon security so I will just point this out.


## CI

.gitlab-ci.yml is set up for small ci show case. The CI process unit-tests the code by kicking up a docker-container and runs pyTest. It's worth noting though that it does not test the designed project as a whole (docker-compose, postgres db etc.) 

## Running the project

**Solution is designed to be run with docker-compose.**

1. Pull/clone the project
2. Open terminal in project base and run the following commands
```
# Build the containers and start them
docker-compose up --build -d 

# Pipe into the app-container
docker exec -i -t c19-africa-test-app-1 bash

# Start the main.py-file
python main.py
```
3. A terminal-meny will present itself. Choose between: 
* running the pipeline,
* running a couple simple unit-tests
